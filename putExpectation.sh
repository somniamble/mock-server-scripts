#! /bin/sh


FILE="$1"
HOST="localhost"
PORT="1080"
URL="http://$HOST:$PORT/mockserver/expectation"
if [ ! -f "$FILE" ]; then
  echo "INVALID FILE SPECIFIED"
  exit 1
else
  echo "curl --header Content-Type: application/json --data @$FILE -X PUT $URL"
fi
curl --header "Content-Type: application/json" --data @"$FILE" -X PUT "$URL"
